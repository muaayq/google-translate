import requests
import re


ss = re.compile(r'\n')

def _txt():
    f1 = open('test1.txt', encoding='utf-8')
    data = f1.read()
    str1 = data.replace('"""""""""','"None"')
    f1 = open('test1.txt', "w",encoding='utf-8')
    f1.write(str1)
    f1.flush()

def Get_word():
    with open('test1.txt',  encoding='utf-8') as f:
        data = f.read()
        ss = re.compile(r'\n')
        data = re.sub(ss, '', data)
        Word = re.compile(r'1 string translation = "(.*?)"',re.S)
        i = re.findall(Word,data)
        return i


def translated_content(text, target_language):
    headers = {
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "content-type": "application/x-www-form-urlencoded;charset=UTF-8",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36"
    }
    # 请求url
    url = "https://translate.google.cn/_/TranslateWebserverUi/data/batchexecute?rpcids=MkEWBc&f.sid=-2609060161424095358&bl=boq_translate-webserver_20201203.07_p0&hl=zh-CN&soc-app=1&soc-platform=1&soc-device=1&_reqid=359373&rt=c"
    # 数据参数
    from_data = {
        "f.req": r"""[[["MkEWBc","[[\"{}\",\"auto\",\"{}\",true],[null]]",null,"generic"]]]""".format(text,
                                                                                                      target_language)
    }
    try:
        r = requests.post(url, headers=headers, data=from_data, timeout=60)
        if r.status_code == 200:
            # 正则匹配结果
            response = re.findall(r',\[\[\\"(.*?)\\",\[\\', r.text)
            if response:
                response = response[0]
            else:
                response = re.findall(r',\[\[\\"(.*?)\\"]', r.text)
                if response:
                    response = response[0]
            return response
    except Exception as e:
        print(e)
        return False



def Translation(word_list):
    for i in word_list:
        response = translated_content(i, 'zh')
        with open('test1.txt', encoding='utf-8') as f:
            data = f.read()
            data = re.sub(ss, '', data)
            str1 = data.replace(i, response)
        with open('test1.txt', "w", encoding='utf-8') as ff:
            ff.write(str1)
            ff.flush()


def t_txt():
    with open('test1.txt', encoding='utf-8') as f:

        line = f.read()
        Word = re.findall(r'1 string translation',line)
        for i in Word:
            str2 = line.replace(i, '\n1 string translation')
            with open('test1.txt', "w", encoding='utf-8') as ff:
                ff.write(str2)
                ff.flush()


def t_t():
    with open('test1.txt', encoding='utf-8') as f:
        line = f.read()
        Word1 = re.findall(r'new,Arcade_Credits', line)
        for x in Word1:
            str3 = line.replace(x, '\nnew,Arcade_Credits')
            with open('test1.txt', "w", encoding='utf-8') as ff:
                ff.write(str3)
                ff.flush()
def main():
    _txt()
    word_list = Get_word()
    Translation(word_list)
    t_txt()
    t_t()

# 翻译
#各国语言编号 ['en', 'zh', 'fr', 'ja', 'de']:
# i = input('请输入翻译内容：')
# response = translated_content(i,'zh')
# print(response)

main()